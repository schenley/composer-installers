<?php

namespace Schenley\Composer;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;

/**
 * Part of the Composer Installers package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Composer Installers
 * @version    1.0.0
 * @author     Schenley Learning
 * @license    MIT License
 * @copyright  (c) 2015, Schenley Learning, LLC
 */

class InstallerPlugin implements PluginInterface
{
    /**
     * {@inheritDoc}
     */
    public function activate(Composer $composer, IOInterface $io)
    {
        $installer = new Installer($io, $composer);

        $composer->getInstallationManager()->addInstaller($installer);
    }
}

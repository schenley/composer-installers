<?php

namespace Schenley\Composer;

use Composer\Package\PackageInterface;

/**
 * Part of the Composer Installers package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Composer Installers
 * @version    1.0.0
 * @author     Schenley Learning
 * @license    MIT License
 * @copyright  (c) 2015, Schenley Learning, LLC
 */

class ThemeInstaller extends BaseInstaller
{
    /**
     * {@inheritDoc}
     */
    public function getPackageBasePath(PackageInterface $package)
    {
        $package = explode('/', $package->getPrettyName());

        $path = str_replace('-', '/', $package[1]);

        $basePath = $this->getPath('public');

        return $basePath.'/themes/'.$path;
    }

    /**
     * {@inheritDoc}
     */
    public function supports($packageType)
    {
        return $packageType == 'schenley-theme';
    }
}
